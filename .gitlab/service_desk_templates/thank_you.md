Thank you for your email. We have recieved your request.

Your request ID is: %{ISSUE_ID}

Issue description:

%{ISSUE_DESCRIPTION}

If you would like to remove yourself from any future emails for this issue, please use the following link:

%{UNSUBSCRIBE_URL}
