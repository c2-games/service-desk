Your request ID is: %{ISSUE_ID}

Reply:

%{NOTE_TEXT}

If you would like to remove yourself from any future emails for this issue, please use the following link:

%{UNSUBSCRIBE_URL}
